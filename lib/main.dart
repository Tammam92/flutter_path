import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build (BuildContext context){
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context)=>Homescreen(),
        '/screen1': (context)=>Screen1(),
        '/screen2':  (context)=>Screen2(),
        '/screen3':  (context)=>Screen3(),
      },
      //home: Homescreen(),



    );
  }
}

class Homescreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("This is HomeScreen"),
      ),
      body: Center(
        child: Row(
          children: [
            ElevatedButton(onPressed:() {Navigator.pushNamed(context, '/screen1');}, child: Text("launch Screen1")),
            ElevatedButton(onPressed:() {Navigator.pushNamed(context, '/screen2');}, child: Text("launch Screen2")),
            ElevatedButton(onPressed:() {Navigator.pushNamed(context, '/screen2');}, child: Text("launch Screen3")),

          ],
        )

      )
    );
  }


}
class Screen1 extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("This is Screen1"),
      ),
      body: Center(
        child: (
          Row(
            children: [
              ElevatedButton(onPressed: (){Navigator.pushNamed(context , '/screen2');}, child: Text("launch screen2")),
              ElevatedButton(onPressed: (){Navigator.pushNamed(context , '/screen3');}, child: Text("launch screen3")),
              ElevatedButton(onPressed: (){Navigator.pushNamed(context , '/');}, child: Text("launch HomeScreen")),
              ElevatedButton(onPressed: (){Navigator.pop(context);}, child: Text("launch Privus scrren")),
            ],
          )
        )
      ),
    );
  }
}

class Screen2 extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("This is Screen1"),
      ),
      body: Center(
          child: (
              Row(
                children: [
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/screen1');}, child: Text("launch screen1")),
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/screen3');}, child: Text("launch screen3")),
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/');}, child: Text("launch Homescreen")),
                  ElevatedButton(onPressed: (){Navigator.pop(context);}, child: Text("launch Privuse Screen")),

                ],
              )
          )
      ),
    );
  }
}

class Screen3 extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("This is Screen3"),
      ),
      body: Center(
          child: (
              Row(
                children: [
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/screen1');}, child: Text("launch screen1")),
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/screen2');}, child: Text("launch screen2")),
                  ElevatedButton(onPressed: (){Navigator.pushNamed(context, '/');}, child: Text("launch HomeScreen")),
                  ElevatedButton(onPressed: (){Navigator.pop(context);}, child: Text("launch Privuse scrren")),

                ],
              )
          )
      ),
    );
  }
}